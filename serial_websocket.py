import base64

from flask import request
from flask_socketio import SocketIO
from serial import Serial, SerialException
from serial.tools import list_ports
from serial.threaded import Protocol, ReaderThread

STM32_VID = 0xF055
STM32_PID = 0x9800

class DeviceNotFound(Exception):
    pass

def get_serial_name():
    for device in list_ports.comports():
        if (device.vid == STM32_VID) and (device.pid == STM32_PID):
            print("found device at {}".format(device.device));
            return device.device
    
    raise DeviceNotFound

class UARTProtocol(Protocol):
    connection_id = None

    def __init__(self):
        pass

    def connection_made(self, transport):
        pass

    def data_received(self, data: bytes):
        sock.emit('uart_data_received', {
            'err' : None,
            'data': base64.b64encode(data).decode('ascii'),
        })

    def connection_lost(self, exc):
        sock.emit('uart_disconnected', {
            'err': str(exc),
        })


uart_connection = None
sock = SocketIO()


@sock.on('connect')
def new_client_connected():
    print("New client connected with SID: {}".format(request.sid))


@sock.on('uart_connect')
def uart_connect(json):
    global uart_connection

    if (uart_connection is not None):
        uart_connection.close()

    try:
        # ser = serial_for_url('loop://', baudrate=115200, timeout=1)
        ser = Serial(get_serial_name(), 115200)
        protocol = ReaderThread(ser, UARTProtocol)
        uart_connection = protocol
        uart_connection.start()
    except SerialException as e:
        return {
            'err': str(e)
        }

    return {
        'err': None
    }


def uart_run_program(data):
    global uart_connection

    if (uart_connection is None):
        ser = Serial(get_serial_name(), 115200)
    else:
        ser = uart_connection

    print(data)

    b = bytearray()
    b.extend(b"\x03\x03")  # Ctrl-C
    b.extend(b"\x01")  # Ctrl-A, enter raw REPL
    b.extend(b"\x04")  # Ctrl-D, soft reboot
    b.extend(data.encode("ascii"))
    b.extend(b"\x04")  # Ctrl-D, execute code

    print("writing program")
    ser.write(b)

    if uart_connection is None:
        ser.close()

    return


@sock.on('uart_write')
def uart_write(data):
    global uart_connection

    print("in writing")
    print(data['data'].encode('ascii'))
    uart_connection.write(data['data'].encode('ascii'))
    print("out writing")

    return {
        'err': None
    }


@sock.on('uart_disconnect')
def uart_disconnect(_):
    global uart_connection

    if uart_connection is None:
        return {
            'err': "you haven't connected"
        }
    uart_connection.close()
    uart_connection = None
