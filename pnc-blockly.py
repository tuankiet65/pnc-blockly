import webbrowser
import threading
import time
import base64
import json
import os

import urllib.request
import urllib.error

from flask import Flask, render_template, request, json as fjson

from serial_websocket import sock, uart_run_program

app = Flask(__name__)
sock.init_app(app)

class Program:
    name = None
    xml = None
    py = None

    def __init__(self, name):
        self.name = name
        if self.name is not None:
            self.filename = base64.urlsafe_b64encode(self.name.encode("utf-8")).decode("utf-8")

    @staticmethod
    def load(name):
        filename = f"programs/{base64.urlsafe_b64encode(name.encode('utf-8')).decode('utf-8')}.json"
        return Program.load_from_file(filename)

    @staticmethod
    def load_from_file(filename):
        try:
            with open(filename, "r") as f:
                decoded = json.load(f)
        except FileNotFoundError as e:
            raise e

        program = Program(decoded['name'])
        program.xml = decoded['xml']
        program.py = decoded['py']

        return program

    def save(self):
        with open(f"programs/{self.filename}.json", "w") as f:
            json.dump({
                "name": self.name,
                "xml" : self.xml,
                "py"  : self.py
            }, f)


@app.route('/')
def hello_world():
    return render_template("index.html")


@app.route("/get_program_list")
def get_program_list():
    result = []
    if not os.path.isdir("programs"):
        os.mkdir("programs")
    for file in os.listdir("programs"):
        if not file.endswith(".json"):
            continue
        try:
            program = Program.load_from_file(f"programs/{file}")
        except RuntimeError:
            continue
        result.append(program.name)

    result.sort()

    return fjson.jsonify(result)


@app.route("/save_program", methods = ["POST"])
def save_program():
    name = request.form["name"]
    try:
        program = Program.load(name)
    except KeyError:
        return "program not found in params", 400
    except FileNotFoundError:
        program = Program(name)

    try:
        program.xml = request.form["xml"]
    except KeyError:
        return "xml not found in params", 400

    try:
        program.py = request.form["py"]
    except KeyError:
        return "py not found in params", 400

    program.save()

    return "ok"

@app.route("/get_program")
def get_program():
    try:
        program = Program.load(request.args["name"])
    except KeyError:
        return "program not found in params", 400
    except FileNotFoundError:
        return "program does not exist", 404

    return fjson.jsonify({
        "name": program.name,
        "xml" : program.xml,
        "py"  : program.py
    })

@app.route("/run_program")
def run_program():
    try:
        program = Program.load(request.args["name"])
    except KeyError:
        return "program not found in params", 400
    except FileNotFoundError:
        return "program does not exist", 404

    uart_run_program(program.py)

    return "ok"

if __name__ == '__main__':
    INDEX_URL = "http://localhost:5000"
    def open_browser():
        with app.test_request_context():
            print('server starting...')
            while True:
                try:
                    urllib.request.urlopen(url = INDEX_URL)
                    break
                except urllib.error.URLError as e:
                    time.sleep(1)
            print('server started !')
            # server started callback
            webbrowser.open_new(INDEX_URL)

    threading.Thread(target=open_browser).start()

    sock.run(app, debug = True)
