import {IDEToolbar, IDEToolbarItem} from "./toolbar";
import {IDETabsManager} from "./tabs";
import {IDEBlockly} from "../blockly/blockly";
import {IDEBackend} from "./backend";
import {IDESelector} from "./selector";
import {IDESerialMonitor} from "./serial";

import Noty from 'noty';

import i18n from '../i18n';

export class IDE {
    constructor(container, backend_url){
        this.container = container;
        this.container.addClass("ide-container");

        var toolbar_container = $("<div class='ide-toolbar-container'>").appendTo(this.container);
        this.toolbar = new IDEToolbar(toolbar_container);
        this.toolbar.add(new IDEToolbarItem("fa-plus", i18n.t("ide.new_program"), this.create_new_tab.bind(this)));

        this.toolbar.add(new IDEToolbarItem("fa-folder-open-o", i18n.t("ide.open"), this.open_program.bind(this)));

        this.toolbar.add(new IDEToolbarItem("fa-floppy-o", i18n.t("ide.save"), function (){
            this.tabs_manager.trigger_save();
        }.bind(this)));

        this.toolbar.add(new IDEToolbarItem("fa-upload", i18n.t("ide.upload"), function (){
            this.tabs_manager.trigger_upload();
        }.bind(this)));

        this.toolbar.add(new IDEToolbarItem("fa-terminal", i18n.t("ide.serial_monitor"), function (){
            for (let tab of this.tabs_manager.tabs) {
                if (tab instanceof IDESerialMonitor) {
                    let noty = new Noty({
                        layout: "bottomRight",
                        theme: "relax",
                        type: "alert",
                        timeout: 5000,
                        text: i18n.t("ide.serial_limit_one_message")
                    }).show();
                    return;
                }
            }

            this.tabs_manager.add(new IDESerialMonitor());
            this.tabs_manager.render();
        }.bind(this)));

        this.toolbar.render();

        var tabs_manager_container = $("<div class='ide-tabs-container'>").appendTo(this.container);
        this.tabs_manager = new IDETabsManager(this, tabs_manager_container);
        this.tabs_manager.render();
    }

    create_new_tab(){
        this.tabs_manager.add(new IDEBlockly(this.tabs_manager));
        this.tabs_manager.render();
    }

    open_program(){
        IDEBackend.get_program_list(function (program_list){
            var file_selector = new IDESelector(program_list, function (program_name){
                this.tabs_manager.add(new IDEBlockly(this.tab_manager, program_name));
            }.bind(this));
        }.bind(this))
    }
}
