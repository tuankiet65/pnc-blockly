import i18n from '../i18n';

export class IDEWelcome {
    constructor(ide) {
        this.ide = ide;
        this.container = $(`<div class="ide-tab-area-buffer">`);

        this.title = i18n.t("ide.welcome.tab_title");

        // signature: function(
        this.on_save = null;
        this.on_close = null;
        this.on_close = null;

    }

    render() {
        var html = $(`
            <div class="ide-tab-welcome align-middle">
                <h1>${i18n.t("ide.welcome.welcome_message")}</h1>
                <button type="button" class="ide-tab-welcome-button btn btn-lg btn-primary" id="ide-tab-welcome-create-new">
                    ${i18n.t("ide.welcome.new_program")}
                </button>
                <h2>${i18n.t("ide.welcome.or")}</h2>
                <button type="button" class="ide-tab-welcome-button btn btn-lg btn-link" id="ide-tab-welcome-open-existing">
                    ${i18n.t("ide.welcome.open_existing")}
                </h1>
            </div>
        `);

        html.find("#ide-tab-welcome-create-new").click(function() {
            this.ide.create_new_tab();
        }.bind(this));

        html.find("#ide-tab-welcome-open-existing").click(function() {
            this.ide.open_program();
        }.bind(this));
        
        this.container.html(html);
    }
}
