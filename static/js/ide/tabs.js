import {
    IDEWelcome
} from "./welcome";

export class IDETab {
    constructor(tabs_manager, title) {
        this.tabs_manager = tabs_manager;

        this.container = $(`<div class="ide-tab-area-buffer">`);

        this.title = title;
    }

    render() {} // not implemented
    on_save() {} // not implemented
    on_upload() {} // not implemented
    on_close() {} // not implemented
}

export class IDETabsManager {
    constructor(ide, container) {
        this.ide = ide;

        this.tabs = [];

        this.active_tab = null;

        this.container = container;

        this.tab_bar = $(`
            <div class='ide-tab-bar'>
                <ul class="nav nav-tabs">
                    <!-- To be appended here -->
                </ul>
            </div>
        `).appendTo(this.container);

        this.tab_area = $("<div class='ide-tab-area'>").appendTo(this.container);
    }

    add(tab_object) {
        tab_object.container.appendTo(this.tab_area);
        tab_object.render();
        this.tabs.push(tab_object);
        this.active_tab = this.tabs.length - 1;
        if (this.render) {
            this.render();
        }
    }

    render_tab_bar() {
        var tab_bar_ul = this.tab_bar.find("ul");
        tab_bar_ul.html("");
        for (var i = 0; i < this.tabs.length; i++) {
            if (!this.tabs[i]) {
                continue;
            }

            var tmp = $(`
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        ${this.tabs[i].title}
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <span class="nav-item-close">
                            &times;
                        </span>
                    </a>
                </li>
            `);

            tmp.find("a.nav-link").click(function(id) {
                return function() {
                    this.active_tab = id;
                    this.render();
                }.bind(this);
            }.bind(this)(i));

            tmp.find("span.nav-item-close").click(function(id) {
                return function() {
                    if (this.tabs[id].on_close) {
                        this.tabs[id].on_close();
                    }
                    
                    this.tabs[id].container.remove();
                    this.tabs[id] = null;

                    if (this.active_tab == id) {
                        for (var i = id + 1; i < this.tabs.length; i++) {
                            if (this.tabs[i]) {
                                this.active_tab = i;
                                this.render();
                                return;
                            }
                        }

                        for (var i = id - 1; i >= 0; i--) {
                            if (this.tabs[i]) {
                                this.active_tab = i;
                                this.render();
                                return;
                            }
                        }

                    }

                    this.render();
                }.bind(this);
            }.bind(this)(i));

            if (i == this.active_tab) {
                tmp.find("a.nav-link").addClass("active");
            }

            tmp.appendTo(tab_bar_ul);
        }
    }

    render_tab_area() {
        this.tab_area.fadeTo(100, 0, function() {
            for (var i = 0; i < this.tabs.length; i++) {
                if (this.tabs[i])
                    this.tabs[i].container.hide();
            }
            this.tabs[this.active_tab].container.show();
            this.tab_area.fadeTo(100, 1);
        }.bind(this));
    }

    render() {
        var has_valid_tabs = false;

        for (var tab of this.tabs) {
            if (tab) {
                has_valid_tabs = true;
                break;
            }
        }

        if (!has_valid_tabs) {
            this.add(new IDEWelcome(this.ide));
        }

        this.render_tab_bar();
        this.render_tab_area();
    }

    trigger_save() {
        if (this.tabs[this.active_tab].on_save) {
            this.tabs[this.active_tab].on_save();
        }
    }

    trigger_upload() {
        if (this.tabs[this.active_tab].on_upload) {
            this.tabs[this.active_tab].on_upload();
        }
    }
}