import i18n from "../../../i18n";

export var BLOCKS = {
    name: "PyBoard",
    id_prefix: "pyb",
    blocks: {}
};

BLOCKS.blocks['delay_millis'] = {
    init: function() {
        this.appendValueInput("delay")
            .setCheck("Number")
            .appendField(i18n.t("pyb.sleep_for"));
        this.appendDummyInput()
            .appendField(i18n.t("pyb.millisecond"));
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(230);
        this.setTooltip('');
        this.setHelpUrl('');
    },

    generator_py: function(block) {
        var value_delay = Blockly.Python.valueToCode(block, 'delay', Blockly.Python.ORDER_ATOMIC);
        var code = `pyb.delay(${value_delay})\n`;
        return code;
    }

};

//////////////////////////////////////////////////////////////////////////////////////

BLOCKS.blocks['delay_micros'] = {
    init: function() {
        this.appendValueInput("delay")
            .setCheck("Number")
            .appendField(i18n.t("pyb.sleep_for"))
        this.appendDummyInput()
            .appendField(i18n.t("pyb.microsecond"));
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(230);
        this.setTooltip('');
        this.setHelpUrl('');
    },

    generator_py: function(block) {
        var value_delay = Blockly.Python.valueToCode(block, 'delay', Blockly.Python.ORDER_ATOMIC);
        var code = `pyb.udelay(${value_delay})\n`;
        return code;
    }
};

//////////////////////////////////////////////////////////////////////////////////////

BLOCKS.blocks['set_freq'] = {
    init: function() {
        this.appendDummyInput()
            .appendField(i18n.t("pyb.set_freq"))
            .appendField(new Blockly.FieldDropdown([
                ["8MHz", "8000000"],
                ["16MHz", "16000000"],
                ["24MHz", "24000000"],
                ["30MHz", "30000000"],
                ["32MHz", "32000000"],
                ["36MHz", "36000000"],
                ["40MHz", "40000000"],
                ["42MHz", "42000000"],
                ["48MHz", "48000000"],
                ["54MHz", "54000000"],
                ["56MHz", "56000000"],
                ["60MHz", "60000000"],
                ["64MHz", "64000000"],
                ["72MHz", "72000000"],
                ["84MHz", "84000000"],
                ["96MHz", "96000000"],
                ["108MHz", "108000000"],
                ["120MHz", "120000000"],
                ["144MHz", "144000000"],
            ]), "frequency");
        this.setColour(230);
        this.setTooltip('');
        this.setHelpUrl('');
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
    },

    generator_py: function(block) {
        var dropdown_frequency = block.getFieldValue('frequency');
        var code = `pyb.freq(${dropdown_frequency})\n`;
        return code;
    }
};

//////////////////////////////////////////////////////////////////////////////////////

// pyb.rng()

//////////////////////////////////////////////////////////////////////////////////////

// pyb.sync()
