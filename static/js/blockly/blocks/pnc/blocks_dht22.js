import i18n from '../../../i18n';

export var BLOCKS = {
    name: "DHT22",
    id_prefix: "pnc_dht22",
    blocks: {}
};

BLOCKS.blocks['get_temperature'] = {
    init: function (){
        this.appendDummyInput()
            .appendField(i18n.t("DHT22.get_temperature"))
            .appendField(new Blockly.FieldDropdown([
                    ["DIGITAL1", "DIGITAL1"],
                    ["DIGITAL2", "DIGITAL2"],
                    ["DIGITAL3", "DIGITAL3"],
                    ["DIGITAL4", "DIGITAL4"]]),
                "port");
        this.setOutput(true, "Number");
        this.setColour(230);
        this.setTooltip("");
        this.setHelpUrl("");
    },

    generator_py: function (block){
        var dropdown_port = block.getFieldValue('port');

        var code = `(pnc.DHT22("${dropdown_port}").read()[0])`;

        return [code, Blockly.Python.ORDER_ATOMIC];
    }
};

BLOCKS.blocks['get_humidity'] = {
    init: function (){
        this.appendDummyInput()
            .appendField(i18n.t("DHT22.get_humidity"))
            .appendField(new Blockly.FieldDropdown([
                    ["DIGITAL1", "DIGITAL1"],
                    ["DIGITAL2", "DIGITAL2"],
                    ["DIGITAL3", "DIGITAL3"],
                    ["DIGITAL4", "DIGITAL4"]]),
                "port");
        this.setOutput(true, "Number");
        this.setColour(230);
        this.setTooltip("");
        this.setHelpUrl("");
    },

    generator_py: function (block){
        var dropdown_port = block.getFieldValue('port');

        var code = `(pnc.DHT22("${dropdown_port}").read()[1])`;

        return [code, Blockly.Python.ORDER_ATOMIC];
    }
};
