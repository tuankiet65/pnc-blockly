import i18n from "../../../i18n";

export var BLOCKS = {
    name: "LCD",
    id_prefix: "pnc_lcd",
    blocks: {}
};

BLOCKS.blocks['init'] = {
    init: function() {
        this.appendDummyInput()
            .appendField(i18n.t("lcd.init"))
            .appendField(new Blockly.FieldDropdown([
                ["UART1", "UART1"],
                ["UART2", "UART2"],
                ["UART3", "UART3"]
            ]), "port");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(230);
        this.setTooltip('');
        this.setHelpUrl('');
    },

    generator_py: function(block) {
        var dropdown_port = block.getFieldValue('port');
        // TODO: Assemble Python into code variable.
        var code = `pnc.LCD_16x2("${dropdown_port}").init()\n`;
        return code;
    }
};

//////////////////////////////////////////////////////////////////////////////////////

BLOCKS.blocks['write'] = {
    init: function() {
        this.appendValueInput("msg")
            .setCheck("String")
            .appendField(i18n.t("lcd.write.write"));
        this.appendDummyInput()
            .appendField(i18n.t("lcd.write.into_lcd"))
            .appendField(new Blockly.FieldDropdown([
                ["UART1", "UART1"],
                ["UART2", "UART2"],
                ["UART3", "UART3"]
            ]), "port");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(230);
        this.setTooltip('');
        this.setHelpUrl('');
    },

    generator_py: function(block) {
        var value_msg = Blockly.Python.valueToCode(block, 'msg', Blockly.Python.ORDER_ATOMIC);
        var dropdown_port = block.getFieldValue('port');
        // TODO: Assemble Python into code variable.
        var code = `pnc.LCD_16x2("${dropdown_port}").write(${value_msg})\n`;
        return code;
    }
};

//////////////////////////////////////////////////////////////////////////////////////

BLOCKS.blocks['set_cursor'] = {
    init: function() {
        this.appendDummyInput()
            .appendField(i18n.t("lcd.set_cursor.turn"))
            .appendField(new Blockly.FieldDropdown([
                [i18n.t("lcd.set_cursor.on"), "True"],
                [i18n.t("lcd.set_cursor.off"), "False"]
            ]), "state")
            .appendField(i18n.t("lcd.set_cursor.cursor_blink"))
            .appendField(new Blockly.FieldDropdown([
                ["UART1", "UART1"],
                ["UART2", "UART2"],
                ["UART3", "UART3"]
            ]), "port");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(230);
        this.setTooltip('');
        this.setHelpUrl('');
    },

    generator_py: function(block) {
        var dropdown_state = block.getFieldValue('state');
        var dropdown_port = block.getFieldValue('port');
        // TODO: Assemble Python into code variable.
        var code = `pnc.LCD_16x2("${dropdown_port}").set_cursor(${dropdown_state})\n`;
        return code;
    }
};

//////////////////////////////////////////////////////////////////////////////////////

BLOCKS.blocks['set_blink'] = {
    init: function() {
        this.appendDummyInput()
            .appendField(i18n.t("lcd.set_blink.turn"))
            .appendField(new Blockly.FieldDropdown([
                [i18n.t("lcd.set_blink.on"), "True"],
                [i18n.t("lcd.set_blink.off"), "False"]
            ]), "state")
            .appendField(i18n.t("lcd.set_blink.cursor_blink"))
            .appendField(new Blockly.FieldDropdown([
                ["UART1", "UART1"],
                ["UART2", "UART2"],
                ["UART3", "UART3"]
            ]), "port");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(230);
        this.setTooltip('');
        this.setHelpUrl('');
    },

    generator_py: function(block) {
        var dropdown_state = block.getFieldValue('state');
        var dropdown_port = block.getFieldValue('port');
        // TODO: Assemble Python into code variable.
        var code = `pnc.LCD_16x2("${dropdown_port}").set_blink(${dropdown_state})\n`;
        return code;
    }
};

//////////////////////////////////////////////////////////////////////////////////////

BLOCKS.blocks['set_backlight'] = {
    init: function() {
        this.appendValueInput("brightness")
            .setCheck("Number")
            .appendField(i18n.t("lcd.set_brightness.set_brightness"))
            .appendField(new Blockly.FieldDropdown([
                ["UART1", "UART1"],
                ["UART2", "UART2"],
                ["UART3", "UART3"]
            ]), "port")
            .appendField(i18n.t("lcd.set_brightness.to"));
        this.setInputsInline(false);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(230);
        this.setTooltip('');
        this.setHelpUrl('');
    },

    generator_py: function(block) {
        var dropdown_port = block.getFieldValue('port');
        var value_brightness = Blockly.Python.valueToCode(block, 'brightness', Blockly.Python.ORDER_ATOMIC);
        // TODO: Assemble Python into code variable.
        var code = `pnc.LCD_16x2("${dropdown_port}").set_backlight(${value_brightness})\n`;
        return code;
    }
};

//////////////////////////////////////////////////////////////////////////////////////

BLOCKS.blocks['clear'] = {
    init: function() {
        this.appendDummyInput()
            .appendField(i18n.t("lcd.clear"))
            .appendField(new Blockly.FieldDropdown([
                ["UART1", "UART1"],
                ["UART2", "UART2"],
                ["UART3", "UART3"]
            ]), "port");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(230);
        this.setTooltip('');
        this.setHelpUrl('');
    },

    generator_py: function(block) {
        var dropdown_port = block.getFieldValue('port');
        // TODO: Assemble Python into code variable.
        var code = `pnc.LCD_16x2("${dropdown_port}").clear()\n`;
        return code;
    }
};


//////////////////////////////////////////////////////////////////////////////////////

BLOCKS.blocks['set_position'] = {
    init: function() {
        this.appendValueInput("column")
            .setCheck("Number")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(i18n.t("lcd.set_position.set_position"))
            .appendField(new Blockly.FieldDropdown([
                ["UART1", "UART1"],
                ["UART2", "UART2"],
                ["UART3", "UART3"]
            ]), "port")
            .appendField(i18n.t("lcd.set_position.column"));
        this.appendValueInput("row")
            .setCheck("Number")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(i18n.t("lcd.set_position.row"));
        this.setInputsInline(false);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(230);
        this.setTooltip('');
        this.setHelpUrl('');
    },

    generator_py: function(block) {
        var dropdown_port = block.getFieldValue('port');
        var value_column = Blockly.Python.valueToCode(block, 'column', Blockly.Python.ORDER_ATOMIC);
        var value_row = Blockly.Python.valueToCode(block, 'row', Blockly.Python.ORDER_ATOMIC);
        // TODO: Assemble Python into code variable.
        var code = `pnc.LCD_16x2("${dropdown_port}").set_position(${value_row}, ${value_column})\n`;
        return code;
    }
};

//////////////////////////////////////////////////////////////////////////////////////
