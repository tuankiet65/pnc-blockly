import i18n from "../../../i18n";

export let BLOCKS = {
    name: "Relay",
    id_prefix: "pnc_relay2x",
    blocks: {}
};

BLOCKS.blocks['relay2x_turn_on'] = {
    init: function (){
        this.appendDummyInput()
            .appendField(i18n.t("relay2x.switch_on"))
            .appendField(new Blockly.FieldDropdown([["1", "1"], ["2", "2"]]), "relay_num")
            .appendField(i18n.t("relay2x.on_port"))
            .appendField(new Blockly.FieldDropdown([
                    ["DIGITAL1", "DIGITAL1"],
                    ["DIGITAL2", "DIGITAL2"],
                    ["DIGITAL3", "DIGITAL3"],
                    ["DIGITAL4", "DIGITAL4"]]),
                "port")
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(230);
        this.setTooltip("");
        this.setHelpUrl("");
    },

    generator_py: function (block){
        var dropdown_relay_num = block.getFieldValue('relay_num');
        var dropdown_port = block.getFieldValue('port');

        var code = `pnc.Relay2x("${dropdown_port}").on(${dropdown_relay_num})\n`;

        return code;
    }
};

BLOCKS.blocks['relay2x_turn_off'] = {
    init: function (){
        this.appendDummyInput()
            .appendField(i18n.t("relay2x.switch_off"))
            .appendField(new Blockly.FieldDropdown([["1", "1"], ["2", "2"]]), "relay_num")
            .appendField(i18n.t("relay2x.on_port"))
            .appendField(new Blockly.FieldDropdown([
                    ["DIGITAL1", "DIGITAL1"],
                    ["DIGITAL2", "DIGITAL2"],
                    ["DIGITAL3", "DIGITAL3"],
                    ["DIGITAL4", "DIGITAL4"]]),
                "port")
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(230);
        this.setTooltip("");
        this.setHelpUrl("");
    },

    generator_py: function (block){
        var dropdown_relay_num = block.getFieldValue('relay_num');
        var dropdown_port = block.getFieldValue('port');

        var code = `pnc.Relay2x("${dropdown_port}").off(${dropdown_relay_num})\n`;

        return code;
    }
};

BLOCKS.blocks['relay2x_toggle'] = {
    init: function (){
        this.appendDummyInput()
            .appendField(i18n.t("relay2x.toggle"))
            .appendField(new Blockly.FieldDropdown([["1", "1"], ["2", "2"]]), "relay_num")
            .appendField(i18n.t("relay2x.on_port"))
            .appendField(new Blockly.FieldDropdown([
                    ["DIGITAL1", "DIGITAL1"],
                    ["DIGITAL2", "DIGITAL2"],
                    ["DIGITAL3", "DIGITAL3"],
                    ["DIGITAL4", "DIGITAL4"]]),
                "port")
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(230);
        this.setTooltip("");
        this.setHelpUrl("");
    },

    generator_py: function (block){
        var dropdown_relay_num = block.getFieldValue('relay_num');
        var dropdown_port = block.getFieldValue('port');

        var code = `pnc.Relay2x("${dropdown_port}").toggle(${dropdown_relay_num})\n`;

        return code;
    }
};

// TODO: relay2x_status
