import i18n from "../../../i18n";

export let BLOCKS = {
    name: "RTC",
    id_prefix: "rtc",
    blocks: {}
};

BLOCKS.blocks['get_time'] = {
    init: function (){
        this.appendDummyInput()
            .appendField(i18n.t("rtc.get_time"))
            .appendField(new Blockly.FieldDropdown([
                    ["I2C1", "I2C1"],
                    ["I2C2", "I2C2"],
                    ["I2C3", "I2C3"],
                    ["I2C4", "I2C4"]]),
                "port");
        this.setOutput(true, "Timestamp");
        this.setColour(230);
        this.setTooltip("");
        this.setHelpUrl("");
    },

    generator_py: function (block){
        var dropdown_port = block.getFieldValue('port');

        var code = `(pnc.RTC_DS1307("${dropdown_port}").get_timestamp())`;

        return [code, Blockly.Python.ORDER_ATOMIC];
    }
};

BLOCKS.blocks['year_of'] = {
    init: function (){
        this.appendValueInput("timestamp")
            .setCheck("Timestamp")
            .appendField(i18n.t("rtc.year_of"));
        this.setOutput(true, "Number");
        this.setColour(230);
        this.setTooltip("");
    },

    generator_py: function (block){
        var value_timestamp = Blockly.Python.valueToCode(block, 'timestamp', Blockly.Python.ORDER_ATOMIC);
        var code = `(utime.localtime(${value_timestamp})[0])`;
        return [code, Blockly.Python.ORDER_ATOMIC];
    }
};

BLOCKS.blocks['month_of'] = {
    init: function (){
        this.appendValueInput("timestamp")
            .setCheck("Timestamp")
            .appendField(i18n.t("rtc.month_of"));
        this.setOutput(true, "Number");
        this.setColour(230);
        this.setTooltip("");
    },

    generator_py: function (block){
        var value_timestamp = Blockly.Python.valueToCode(block, 'timestamp', Blockly.Python.ORDER_ATOMIC);
        var code = `(utime.localtime(${value_timestamp})[1])`;
        return [code, Blockly.Python.ORDER_ATOMIC];
    }
};

BLOCKS.blocks['day_of'] = {
    init: function (){
        this.appendValueInput("timestamp")
            .setCheck("Timestamp")
            .appendField(i18n.t("rtc.day_of"));
        this.setOutput(true, "Number");
        this.setColour(230);
        this.setTooltip("");
    },

    generator_py: function (block){
        var value_timestamp = Blockly.Python.valueToCode(block, 'timestamp', Blockly.Python.ORDER_ATOMIC);
        var code = `(utime.localtime(${value_timestamp})[2])`;
        return [code, Blockly.Python.ORDER_ATOMIC];
    }
};

BLOCKS.blocks['hour_of'] = {
    init: function (){
        this.appendValueInput("timestamp")
            .setCheck("Timestamp")
            .appendField(i18n.t("rtc.hour_of"));
        this.setOutput(true, "Number");
        this.setColour(230);
        this.setTooltip("");
    },

    generator_py: function (block){
        var value_timestamp = Blockly.Python.valueToCode(block, 'timestamp', Blockly.Python.ORDER_ATOMIC);
        var code = `(utime.localtime(${value_timestamp})[3])`;
        return [code, Blockly.Python.ORDER_ATOMIC];
    }
};

BLOCKS.blocks['minute_of'] = {
    init: function (){
        this.appendValueInput("timestamp")
            .setCheck("Timestamp")
            .appendField(i18n.t("rtc.minute_of"));
        this.setOutput(true, "Number");
        this.setColour(230);
        this.setTooltip("");
    },

    generator_py: function (block){
        var value_timestamp = Blockly.Python.valueToCode(block, 'timestamp', Blockly.Python.ORDER_ATOMIC);
        var code = `(utime.localtime(${value_timestamp})[4])`;
        return [code, Blockly.Python.ORDER_ATOMIC];
    }
};

BLOCKS.blocks['second_of'] = {
    init: function (){
        this.appendValueInput("timestamp")
            .setCheck("Timestamp")
            .appendField(i18n.t("rtc.second_of"));
        this.setOutput(true, "Number");
        this.setColour(230);
        this.setTooltip("");
    },

    generator_py: function (block){
        var value_timestamp = Blockly.Python.valueToCode(block, 'timestamp', Blockly.Python.ORDER_ATOMIC);
        var code = `(utime.localtime(${value_timestamp})[5])`;
        return [code, Blockly.Python.ORDER_ATOMIC];
    }
};

BLOCKS.blocks['weekday_of'] = {
    init: function (){
        this.appendValueInput("timestamp")
            .setCheck("Timestamp")
            .appendField(i18n.t("rtc.weekday_of"));
        this.setOutput(true, "Number");
        this.setColour(230);
        this.setTooltip("");
    },

    generator_py: function (block){
        var value_timestamp = Blockly.Python.valueToCode(block, 'timestamp', Blockly.Python.ORDER_ATOMIC);
        var code = `(utime.localtime(${value_timestamp})[6])`;
        return [code, Blockly.Python.ORDER_ATOMIC];
    }
};

BLOCKS.blocks['yearday_of'] = {
    init: function (){
        this.appendValueInput("timestamp")
            .setCheck("Timestamp")
            .appendField(i18n.t("rtc.yearday_of"));
        this.setOutput(true, "Number");
        this.setColour(230);
        this.setTooltip("");
    },

    generator_py: function (block){
        var value_timestamp = Blockly.Python.valueToCode(block, 'timestamp', Blockly.Python.ORDER_ATOMIC);
        var code = `(utime.localtime(${value_timestamp})[7])`;
        return [code, Blockly.Python.ORDER_ATOMIC];
    }
};

BLOCKS.blocks['duration'] = {
    init: function (){
        this.appendValueInput("ts1")
            .setCheck("Timestamp")
            .appendField(i18n.t("rtc.duration.duration_from"));
        this.appendValueInput("ts2")
            .setCheck("Timestamp")
            .appendField(i18n.t("rtc.duration.to"));
        this.setInputsInline(true);
        this.setOutput(true, "Number");
        this.setColour(230);
        this.setTooltip("");
        this.setHelpUrl("");
    },

    generator_py: function (block){
        var value_ts1 = Blockly.Python.valueToCode(block, 'ts1', Blockly.Python.ORDER_ATOMIC);
        var value_ts2 = Blockly.Python.valueToCode(block, 'ts2', Blockly.Python.ORDER_ATOMIC);
        
        var code = `utime.ticks_diff(${value_ts1}, ${value_ts2})`;
        
        return [code, Blockly.Python.ORDER_NONE];
    }
};
