var path = require('path');
var webpack = require('webpack');
var UglifyJSPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
    entry: [
        'font-awesome-webpack',
        'bootstrap-loader',
        './js/main.js'
    ],

    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: "/static/dist/"
    },

    plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "Util": "exports-loader?Util!bootstrap/js/dist/util"
        }),

        new UglifyJSPlugin({
            sourceMap: true
        })
    ],

    devtool: 'source-map',

    module: {
        rules: [{
            test: /\.js$/,
            exclude: /(node_modules|bower_components)/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ['env']
                }
            }
        }, {
            test: /\.css$/,
            use: ['style-loader', 'css-loader']
        }, {
            test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
            use: {
                loader: 'url-loader',
                options: {
                    mimetype: "application/font-woff",
                    limit: 32768
                }
            }
        }, {
            test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
            use: {
                loader: 'file-loader',
            }
        }, {
            test: /\/node_modules\/blockly/,
            use: {
                loader: 'script-loader',
            }
        }, {
            test: /locales/,
            use: {
                loader: '@alienfast/i18next-loader',
                options: {
                    basenameAsNamespace: true
                }
            }
        }]
    }
};
